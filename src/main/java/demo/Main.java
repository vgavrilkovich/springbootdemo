package demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
@Configuration(value = "demo.config")
public class Main extends SpringBootServletInitializer {

    public static void main(String[] args) throws Exception {
        new SpringApplication(Main.class).run(args);
    }
}
