package demo.model;

import demo.db.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class TaskController {
    @Autowired
    private TaskDao taskDao;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public ModelAndView indexPage() {
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("taskList", taskDao.listAll());
        return mav;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ModelAndView search(@RequestParam String username) {
        List<Task> taskList = null;
        if (username != null && !username.isEmpty()) {
            taskList = taskDao.search(username);
        } else {
            taskList = taskDao.listAll();
        }
        ModelAndView mav = new ModelAndView("index");
        mav.addObject("taskList", taskList);
        return mav;
    }
}
