package demo.model;

import demo.db.Task;
import demo.db.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TaskDao {
    @Autowired
    private SessionFactory sessionFactory;

    public List<Task> listAll() {
        Session session = sessionFactory.getCurrentSession();
        CriteriaQuery query = session.getCriteriaBuilder().createQuery(Task.class);
        query.select(query.from(Task.class));
        return session.createQuery(query).getResultList();
    }

    public List<Task> search(String username) {
        Session session = sessionFactory.getCurrentSession();
        session.beginTransaction();
        Query query = session.createQuery("select t from " + Task.class.getName() + " t join " + User.class.getName() + " u on(u.id = t.user) where u.name = :username", Task.class);
        query.setParameter("username", username);
        ArrayList<Task> taskList = new ArrayList<>(query.list());
        session.getTransaction().commit();
        return taskList;
    }
}
