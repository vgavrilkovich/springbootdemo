package demo.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
public class CustomErrorController implements ErrorController {
    private final ErrorAttributes errorAttributes;

    @Autowired
    public CustomErrorController(ErrorAttributes errorAttributes) {
        Assert.notNull(errorAttributes, "ErrorAttributes must not be null");
        this.errorAttributes = errorAttributes;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping("/error")
    @ResponseBody
    public String processError(HttpServletRequest request, WebRequest webRequest) {

        Integer statusCode = (Integer)request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        if(statusCode.intValue() == 500) {
            return "error";
        } else {
            String message = (String)request.getAttribute(RequestDispatcher.ERROR_MESSAGE);
            Exception exception = (Exception)request.getAttribute(RequestDispatcher.ERROR_EXCEPTION);
            Map<String, Object> body = errorAttributes.getErrorAttributes(webRequest, true);
            String trace = (String) body.get("trace");
            StringBuffer retBuf = new StringBuffer();
            retBuf.append("<pre>");

            if(statusCode != null) {
                retBuf.append("Status Code : ");
                retBuf.append(statusCode);
            }

            if(message != null && message.trim().length() > 0) {
                retBuf.append("\n\rError Message : ");
                retBuf.append(message);
            }

            if(trace != null) {
                retBuf.append("\n\rStack Trace : ");
                retBuf.append(trace);
            }

            retBuf.append("</pre>");

            return retBuf.toString();
        }

    }
}
