<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01
Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Cписок задач</title>
</head>
<body>
<div align="center">
    <h2></h2>
    <form method="get" action="search">
        <p><b>Имя пользователя</b><br>
        <input type="text" name="username"/>
                <input type="submit" value="OK" />
    </form>
    <table border="1" cellpadding="5">
        <tr>
            <th>ID задачи</th>
            <th>Имя пользователя</th>
            <th>Задача</th>
            <th>Дата назначения</th>
        </tr>
        <c:forEach items="${taskList}" var="task">
            <tr>
                <td>${task.id}</td>
                <td>${task.user.name}</td>
                <td>${task.name}</td>
                <td>${task.date}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>